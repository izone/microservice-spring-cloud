package com.leeyns.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.leeyns.entity.User;

@RestController
public class MoveController {
	
	@Resource
	private RestTemplate restTemplate;

	@GetMapping("/simple/{id}")
	public User getUser(@PathVariable Long id){
		return restTemplate.getForObject("http://localhost:7900/simple/"+id,User.class);
	}
}
