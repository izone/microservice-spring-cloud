package com.leeyns.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.leeyns.entity.User;
import com.leeyns.repository.UserRepository;

@RestController
public class UserController {
	
	@Resource
	private UserRepository userRepository;
	
	@GetMapping("/simple/{id}")
	public User findById(@PathVariable Long id){
		return userRepository.findOne(id);
	}
}
